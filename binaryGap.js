// positive integer
// N [1, 2^31]
// Time: O(log(N))
// Space: O(1)
// Time Coding: 17 min.
function solution(N) {
    var binaryStr = N.toString(2)
    var currentGap = 0;
    var maxGap = 0;
    for(var i = 0; i < binaryStr.length; i++){
           var currentValue = binaryStr[i];
           if(currentValue === "0"){
               currentGap++;
           }
           else if(currentValue === "1"){
                if(currentGap > maxGap){
                    maxGap = currentGap;   
                }
                currentGap = 0;
           }
    }
    return maxGap;
}