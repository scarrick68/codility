/*
	Score: 100% correctness, 100% performance
	Time: 13 minutes
	Test Cases:
		- [0, 340, 17]
		- [11, 345, 17]
		- [1, 12, 2]
		- [0, 12, 2]
*/

function solution(A, B, K) {
    // write your code in JavaScript (Node.js 4.0.0)
    // find first number after A that is divisible by K
    // find last number before B divisible by K
    // reset the range and divide
    var count = 0;
    var firstDiv = Math.ceil(A / K) * K;
    var lastDiv = Math.floor(B / K) * K;

    count = (lastDiv / K) - (firstDiv / K) + 1;
    
    return count;
}