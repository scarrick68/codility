// got 75% on this. need to figure out the
// edge cases i'm missing.

// testing notes:
// 1. array can be empty
// 2. rotation can be 0 turns
// 3. element is [-1000, 1000] (shouldn't really matter)

function solution(A, K) {
    // write your code in JavaScript (Node.js 4.0.0)
    // store value of shifted index with temp
    // replace value at shifted index with current value
    // move to shifted index
    // calculate shifted index based on new position
    // store temp there
    var index = 0;
    var shiftedElements = 0;
    var length = A.length;
    var temp1 = A[index];
    var temp2 = A[(index + K) % length];
    while(shiftedElements < length){
        A[(index + K) % length] = temp1;
        index = index + K;
        temp1 = temp2;
        temp2 = A[(index + K) % length];
        shiftedElements++;
    }
    return A;
}