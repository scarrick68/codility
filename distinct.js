/*
    Score: 100% correctness, 100% performance
    Time: 10 minutes with strategy description / notes
    Notes: Codility says detected time complexity is O(N*log(N)),
    but that shouldn't be based on the code. Other constant time
    operations are known to affect this.
*/


function solution(A) {
    // write your code in JavaScript (Node.js 4.0.0)
    // A can be empty
    // Problem says O(N*log(N))) time and O(N) space, but I can do O(N) time and O(N) space
    // by using an object as a hash. Object as hash requires at most 100,000. Counting array
    // would require 1,000,000 elements. Idk how JS handles these empty positions (if there is memory cost).
    /*
        Test Cases:
            - []
    */
    
    var uniqueVals = {};
    uniqueVals.length = 0;
    for(var i = 0; i < A.length; i++){
        if(!uniqueVals.hasOwnProperty(A[i])){
            uniqueVals[A[i]] = 1                // setting it equal to 1 is arbitrary. I just need the key to be A[i] for future O(1) lookups
            uniqueVals.length++;
        }
    }
    
    return uniqueVals.length;
}