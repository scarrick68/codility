function solution(X, A) {
    // write your code in JavaScript (Node.js 4.0.0)
    // Leaves may never fill all positions
    // index = time in seconds
    // A[i] = position of leaf
    // A[i] in [1...X]
    var countObj = {}
    countObj.length = 0;
    for(var i = 0; i < A.length; i++){
        if(!countObj[A[i]]){
            countObj[A[i]] = true;
            countObj.length++;
            if(countObj.length === X){
                return i;
            }
        }
    }
    return -1;
}