// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

/*
        Score: 60% correctness, 100% performance, 75% overall (so far)
        time: hour and a half
*/

function solution(S, P, Q) {
    // write your code in JavaScript (Node.js 4.0.0)
    // keep prefix sum of each nucleotide at each position in the array
    // use an array for each nucleotide, taking 4*N additional space
    // calculate sum of each nucleotide for a range in O(1) time
    // check progressively higher impact value nucleotides until one is found
    
    var counters = initCounters(S);
    counters = nucPreSum(S, counters);
    var results = queryResults(S, P, Q, counters);
    
    return results;
}

// initialize the counting arrays
function initCounters(S){
    var counters = {aNuc: [0],
                cNuc: [0],
                gNuc: [0],
                tNuc: [0]};

    if(S[0] === 'A'){
        counters.aNuc[0] = 1;
    }
    if(S[0] === 'C'){
        counters.cNuc[0] = 1;   
    }
    if(S[0] === 'G'){
        counters.gNuc[0] = 1;   
    }
    if(S[0] === 'T'){
        counters.tNuc[0] = 1;   
    }

    return counters;
}

// calculate prefix sums for each nucleotide
function nucPreSum(S, counters){
    for(var i = 1; i < S.length; i++){
        if(S[i] === 'A'){
            counters.aNuc[i] = counters.aNuc[i-1] + 1;          // this was found so ad 1 to running total
            counters.cNuc[i] = counters.cNuc[i-1];              // put zero at i for nucleotides not found
            counters.gNuc[i] = counters.gNuc[i-1];
            counters.tNuc[i] = counters.tNuc[i-1];
        }
        else if(S[i] === 'C'){
            counters.cNuc[i] = counters.cNuc[i-1] + 1;
            counters.gNuc[i] = counters.gNuc[i-1];
            counters.tNuc[i] = counters.tNuc[i-1];
            counters.aNuc[i] = counters.aNuc[i-1];
        }
        else if(S[i] === 'G'){
            counters.gNuc[i] = counters.gNuc[i-1] + 1;
            counters.aNuc[i] = counters.aNuc[i-1];
            counters.cNuc[i] = counters.cNuc[i-1];
            counters.tNuc[i] = counters.tNuc[i-1];
        }
        else{
            counters.tNuc[i] = counters.tNuc[i-1] + 1;
            counters.cNuc[i] = counters.cNuc[i-1];
            counters.gNuc[i] = counters.gNuc[i-1];
            counters.aNuc[i] = counters.aNuc[i-1];
        }
    }

    return counters;
}

function queryResults(S, P, Q, counters){
    var results = [];
    for(i = 0; i < P.length; i++){
        if(Q[i] - P[i] === 0){
            results[i] = singleNuc(S[Q[i]]);
        }
        else if(counters.aNuc[Q[i]] - counters.aNuc[P[i]] > 0){
            results[i] = 1;
        }
        else if(counters.cNuc[Q[i]] - counters.cNuc[P[i]] > 0){
            results[i] = 2;
        }
        else if(counters.gNuc[Q[i]] - counters.gNuc[P[i]] > 0){
            results[i] = 3;   
        }
        else{
            results[i] = 4;   
        }
    }

    return results;
}

function singleNuc(nuc){
    if(nuc === 'A'){
        return 1;   
    }
    else if(nuc === 'C'){
        return 2;
    }
    else if(nuc === 'G'){
        return 3;   
    }
    else{
        return 4;   
    }
}