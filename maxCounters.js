// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

// 100% (correctness and performance)
// 48 minutes with testing

function solution(N, A) {
    // write your code in JavaScript (Node.js 4.0.0)
    // N counters
    // increase(X) for counter X
    // max counter for all
    // array A of M integers reps operations
    // 1<=x<=N => increase(x)
    // counting array is 1 indexed
    // A[i] [1, N+1]
    
    // have an array with N elements
    // addressing them will be i-1
    // problem is how to set all to max efficiently
    // looping over counters would cause N*M in worst case
    // but having maxCounter() all the time results in no change
    // so track max, only do maxCounter when there is a change
    var max = 0;
    var calledMax = -1;
    var counters = new Array(N).fill(0);
    for(var i = 0; i < A.length; i++){
        // increment counter at X
        if(A[i] <= N){
            counters[A[i]-1] = counters[A[i]-1]+1;
            // update max
            if(counters[A[i]-1] > max){
                max = counters[A[i]-1];
            }
        }
        else{
            // call maxCounter() if you didn't just do that
            if(calledMax !== (i-1)){
               counters = maxCounter(max, counters);
               calledMax = i;
            }
            calledMax = i;
        }
    }
    return counters
}

function maxCounter(max, arr){
    for(var i = 0; i < arr.length; i++){
        arr[i] = max;   
    }
    return arr;
}