// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

/*
	Score: correctness 100%, performance 100%
	Time: 8 minutes
*/

function solution(A) {
    // write your code in JavaScript (Node.js 4.0.0)
    // Don't need to handle empty input A.length [3, 100k]
    A.sort(function(a,b){return a-b});
    var end = A.length - 1;
    var allPos = A[end] * A[end-1] * A[end-2];
    var someNeg = A[0] * A[1] * A[end]          // handle the case where there may be large negative numbers. There must be 2 so they end up positive, and then multiply that result by the largest positive number.
    return allPos > someNeg ? allPos : someNeg; 
}