/*
	This one took me a while. It was really easy with the insight
	that any minimal slice has to have 2 or 3 elements (the problem
	requires slices of at least 2 elements). Unfortunately, I had
	to google for that insight.
*/



/*
	Score: 100% correctness, 100% performance
    Test Cases:
    - [0, 1, 0, 2]
    - [0, 0, 1, 2, -1, -3, -3]
    - [1, 1, 2]
*/

function solution(A) {
    // write your code in JavaScript (Node.js 4.0.0)
    var minAvg = Number.MAX_SAFE_INTEGER;
    var minAvgPos = 0;
    var twoSlice = 0;
    var threeSlice = 0;
    for(var i = 0; i<A.length; i++){
        if((i + 1) < A.length){
            twoSlice = (A[i] + A[i+1]) / 2;
            if(twoSlice < minAvg){
                minAvg = twoSlice;
                minAvgPos = i;
            }
        }
        if((i + 2) < A.length){
            threeSlice = (A[i] + A[i+1] + A[i+2]) / 3;
            if(threeSlice < minAvg){
                minAvg = threeSlice;
                minAvgPos = i;
            }
        }
    }
    
    return minAvgPos;
}