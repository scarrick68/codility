// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

// Score: 100% (correctness and performance)
// Time: 23 minutes

function solution(A) {
    // write your code in JavaScript (Node.js 4.0.0)
    // Create counting obj
    // add positive elements to it
    // loop from 1... A.length
    // check for i as property on the object
    var countingObj = {};
    for(var i = 0; i < A.length; i++){
        if(A[i] > 0){
            countingObj[A[i]] = true;   
        }
    }
    
    for(i = 1; i <= A.length; i++){
        if(!countingObj.hasOwnProperty(i)){
            return i;
        }
    }
    return A.length + 1;
}