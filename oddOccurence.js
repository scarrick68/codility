// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(A) {
    // write your code in JavaScript (Node.js 4.0.0)
    // array has odd length
    // A[i] > 0 for all i
    // only one number has odd occurence
    var matches = {};
    for(var i = 0; i < A.length; i++){
        if(matches.hasOwnProperty([A[i]])){
            delete matches[A[i]];   
        }
        else{
            matches[A[i]] = i;   
        }
    }
    var oddOccurence;
    for (var x in matches){
        oddOccurence = parseInt(x);
    }
    // var oddOne = Object.entries(matches); function not available in Node 4.0.
    return oddOccurence;
    
}