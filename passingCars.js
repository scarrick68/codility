/*
	Performance: 100%
	Correctness: 100%
	Time: 16.5 minutes (including strategy notes and test cases)
*/

/*
    Test Cases:
        [1,1,1,1,1,0]
        [0,1,1,1,1,1]
        [1]
        [0]
        [1,0]
        [0,1]
        [1,1,0,1,1]
    
    Strategy:
    // add up all the ones in the array
    // subtract 1 from total every time you find one
    // add current remaining total to running total when you hit a 0
*/

function solution(A) {
    // write your code in JavaScript (Node.js 4.0.0)
    
    var total = 0;
    var passingCars = 0;
    for(var i = 0; i < A.length; i++){
        if(A[i] === 1){
            total++;
        }
    }
    
    for(i = 0; i < A.length; i++){
        if(A[i] === 1){
            total--;   
        }
        if(A[i] === 0){
            passingCars = passingCars + total;
            // Instruction say return -1 if passingCars exceeds a billion. No point in finishing a very large input.
            if(passingCars > 1000000000){
                return -1;   
            }
        }
    }
    
    return passingCars;
}