// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

// Score: 100% (correctness and performance)
// Time: 15 minutes

function solution(A) {
    // write your code in JavaScript (Node.js 4.0.0)
    // keep track of max, that will be N
    // use a counting object to count occurences of each element
    // if an element appears more than once or not at all, return 0
    var max = 0;
    var countObj = {}
    countObj.length = 0;
    for(var i = 0; i < A.length; i++){
        if(A[i] > max){
            max = A[i];   
        }
        // duplicate is found
        if(countObj.hasOwnProperty(A[i])){
            return 0;   
        }
        countObj[A[i]] = 1;
        countObj.length++;
    }
    // missing element
    if(max !== countObj.length){
        return 0;   
    }
    return 1;
}