// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

// 100% correctness and performance.
// could have used reduce instead of first for loop
// Time: 19 minutes including some testing

function solution(A) {
    // write your code in JavaScript (Node.js 4.0.0)
    var total = 0;
    for(var i = 0; i <A.length; i++){
        total = total + A[i];   
    }
    
    var partialSum = 0;
    part1 = 0;
    part2 = total;
    var min = Number.MAX_SAFE_INTEGER
    var diff = 0; 
    for(i = 0; i < A.length-1; i++){
        part1 = part1 + A[i];
        part2 = part2 - A[i];
        diff = Math.abs(part1 - part2);
        if(diff < min){
            min = diff;   
        }
    }
    
    return min;
}