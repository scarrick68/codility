// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

/*
    Test Cases:
        - []
        - [-1, -3, -5]
        - [-1, 3, -5]

    Score: 100% correctness, 100% performance
    Time: ~18 minutes, see notes
    Notes: I originally tried by testing for (a+b) < c || (a+c) < b || (b+c) < a
    and returning 0, else 1. However, that failed one test in each of the test groups
    and resulted in a 0%. All my test cases and the example cases passed, so I'll have
    to look into why that happened. I must have missed a case. My initial approach took
    about 18 minutes and then the redo took ~1 minute to change the comparisons.
*/

function solution(A) {
    // write your code in JavaScript (Node.js 4.0.0)
    // A is not empty
    // 0 <= p < q < r
    A.sort(function(a, b){return a-b})
    var triangle = 0;
    for(var i = 0; i < A.length - 2; i++){
        triangle = isTriangular(A[i], A[i+1], A[i+2]);
        if(triangle === 1){
            return triangle;   
        }
    }
    
    return triangle;
}

function isTriangular(a, b, c){
    if((a + b) > c && (b + c) > a && (a + c) > b){
        return 1;
    }
    else{
        return 0;
    }
}


// Original Attempt